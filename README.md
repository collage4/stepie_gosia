# naglowek powiekszony
to jest pierwszy paragraf
to jest drugi paragraf
to jest trzeci paragraf
*kursywa*
**pogrubienie**
~~przekreslenie~~
>"cytat"
1. Pierwszy element
    1. A to jego podpunkt
2. Drugi element
    2. A to jego podpunkt
- Element pierwszy
    - podpunkt pierwszy
- Element drugi
    - podpunkt drugi
```python
def hello_world():
    print("Hello, World!")

hello_world()
```
'print("Hello, World!").'
![trening](trening.jpeg)
